(function() {

    module.exports = [{
        code: "FREESTAR1",
        value: 100,
        expiry: null,
        count: 100
    }, {
        code: "MYSTAR56",
        value: 25,
        expiry: new Date(2017, 03, 10, 00, 00, 00, 00),// Coupon expires on 10 - March 2017 at midnight
        count: 3
    }]

})()