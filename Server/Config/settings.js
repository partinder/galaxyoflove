"use strict"
var moment = require("moment")

var env = process.env.NODE_ENV

var settings = {
    env: env,
    starRadius: {
        "IN": [ {
            radius: 2,
            value: 99,
            currency: "₹",
            starSize: "Tiny"
        }, {
            radius: 4,
            value: 499,
            currency: "₹",
            starSize: "Small"

        }, {
            radius: 8,
            value: 899,
            currency: "₹",
            starSize: "Big"
        }],
        "US": [{
            radius: 2,
            value: 2.99,
            currency: "$",
            starSize: "Tiny"
        }, {
            radius: 4,
            value: 4.99,
            currency: "$",
            starSize: "Small"

        }, {
            radius: 8,
            value: 9.99,
            currency: "$",
            starSize: "Big"
        }]
    },
    userPicsServerPath: "//d44k5f1zplbpz.cloudfront.net/userPics/",
    awsAccessKeyId: "AKIAJ7JOKDUP5WXTYCCQ",
    awsSecretAccessKey: "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",
    starBorder: 2,
    sendgridKey: "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",
    googleApiKey: "AIzaSyAM0jeQ5aJp1ac4lIgoMT_aY9N6jhoQcJg",
    golApiGetKey: "6lq6yVp2TmG5ZDOSMZmFgHnFw",
    golApiPostKey: "FZL2h5twiQhGBGvzhyDwjsessn771d6jOOgD0a7FRqpCmhK7Xn",
    couponApiKey: "JKGFJkj7676%$jhdga*",
    insta_api_key: 'e4ffe780437da27e48b5032b507a43c9',
    insta_auth_key: '76d4bc891d92f9ff7ad5eae6d6bf918f',
    insta_host: 'test',
    insta_redirect_host: "https://www.galaxyoflove.co",
    paypal_client_id: "AeAdv9D5SiSnMHm7B3pAuAfI8pYTqHkVetKo9mApnVORJKTyWtCiC3iFdmx-a1TSWYapbEExwiRVBxr8",
    paypal_secret: "EDvryLCnFnl9f7VeNFY3Z253KgEiEU5XRiIyCrgKBABOn0DIIPk98I02T5oALhoF2Fg-SWkP9rmAKqDI",
    paypal_mode : "sandbox",
    timeout : 0,
    dbServer : "localhost",
    hostPort : "3030",
    hostServer : "0.0.0.0",
    db : "db_gol",
    serverUrl : "http://localhost:3030"

}

// Development Settings
if (env == "development") {
    settings.serverUrl = "https://dev.galaxyoflove.co"
} else if (env == "production") {

    // Production Settings
    settings.hostServer = "localhost"
    settings.serverUrl = "https://www.galaxyoflove.co"
    settings.insta_api_key = 'd25acf1cc882dc612914f2f4fa8ed681'
    settings.insta_auth_key = 'ad5348511373ddecdbe918062d040bbc'
    settings.insta_host = 'www'
    settings.paypal_client_id = "AReJ2nLsakjk9ntAPcW_8ubb4-koad6YU20gG6ZQXDCtYp61j4W6HLvMUnb-5gvIWT6xMTXT6We_5FXE"
    settings.paypal_secret = "EDpgUhYO3wXOmy3dyDZ_Na3imBq0si9U8l2rUrQf0wf2b-WO0lqFkQXPvx7ZPcYQueW-z6Axoy7aYML4"
    settings.paypal_mode = "live"

}

module.exports = settings