var Boom = require("boom");
var Joi = require("joi");
var starArray = require("../Scripts/stars");
var db = require("../Utils/mongoUtil").getDb();
var _ = require("underscore");
var multiparty = require("multiparty");
var fs = require("fs");
var path = require("path");
var ObjectID = require("mongodb").ObjectID;
var settings = require("../Config/settings");
var moment = require("moment");
var easyimg = require('easyimage');
var randomstring = require("randomstring");
var request = require("request");
var fileUploader = require("../Utils/s3Uploader.js");
var mailer = require("../Utils/mailer");
var helpers = require("../Utils/helpers");
var Boom = require("boom");
var _ = require("underscore");
var Joi = require("joi");
var razorNode = require("razorpay").razorNode;
var couponUtil = require("../Utils/couponUtil");
var joiSchemas = require("../Utils/joiSchemas");
var errors = require("../Utils/errors");

var routes = [{
    // Incompitable Page
    method: "GET",
    path: "/incompatible",
    handler: function(request, reply) {
        reply.file("public/incompatible.html");
    },
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.restrict }
        }
    }
}, {
    // Mobile Page
    method: "GET",
    path: "/mobile",
    handler: function(request, reply) {
        console.log("Replying with Mobile Page");
        reply.file("public/mobile.html");
    }
}, {
    method: "GET",
    path: "/api/v1/home",
    handler: homeHandler,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        },
        validate: {
            query: {
                path: joiSchemas.homeQuery
            }
        }
    }
}, {
    // All assests
    method: "GET",
    path: "/public/{p*}",
    handler: {
        directory: {
            path: "public",
            listing: false
        }
    }

}, {
    method: "POST",
    path: "/api/v1/uploadFiles/{imageType}",
    config: {
        payload: {
            maxBytes: 209715200,
            output: 'stream',
            parse: false
        },
        handler: uploadFiles,
    }
}, {
    method: "POST",
    path: "/api/v1/createStar",
    handler: createStar,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        },
        validate: {
            payload: {
                starData: joiSchemas.starData,
                paymentData: joiSchemas.paymentData,
                captchaResponse: joiSchemas.captchaResponse
            },
            failAction: function(request, reply, source, error) {
                console.log(error);
                console.log(request.payload);
                errors.relayError({
                    msg: 'Error in Payload validation, createStar',
                    err: request.payload
                });
            }
        }
    }
}, {
    method: "GET",
    path: "/api/v1/starNameCheck",
    handler: starNameCheck,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        },
        validate: {
            query: joiSchemas.starName
        }
    }
}, {
    method: "POST",
    path: "/api/v1/validateStar",
    handler: validateStar,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        },
        validate: {
            payload: joiSchemas.validateStar
        }
    }
}, {
    method: "GET",
    path: "/api/v1/starData",
    handler: starHandler,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        },
        validate: {
            query: joiSchemas.starName
        }
    }
}, {
    method: "GET",
    path: "/api/v1/validatePayment",
    handler: verifyPayment,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        },
        validate: {
            query: joiSchemas.starName
        }
    }
}, {
    method: "POST",
    path: "/api/v1/validateCoupon",
    handler: validateCouponHandler,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        }
    }
}, {
    method: "GET",
    path: "/api/v1/static",
    handler: staticDataHandler,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        }
    }

}, {
    method: 'POST',
    path: '/api/v1/shareCreatedStar',
    handler: shareCreatedStarHandler,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        }
    }
}, {
    method: 'POST',
    path: '/api/v1/emailQuery',
    handler: emailQueryHandler,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.authCheck }
        }
    }
}, {
    method: "POST",
    path: "/api/v1/coupons",
    handler: couponHandler,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.couponAuthCheck }
        },
        validate: {
            payload: {
                coupons: joiSchemas.coupons
            }
        }
    }
}, {
    method: "GET",
    path: "/api/v1/coupons",
    handler: couponHandler,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.couponAuthCheck }
        },
        validate: {
            query: joiSchemas.couponQuery
        }
    }
}, {
    method: '*',
    path: '/{p*}', // catch-all path
    handler: cathcAllPath,
    config: {
        ext: {
            onPreHandler: { method: helpers.authFuncs.restrict }
        }
    }
}];

module.exports = routes;

function cathcAllPath(request, reply) {

        var ua = request.headers['user-agent'].toLowerCase();
        var isMobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(ua) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(ua.substr(0, 4));
        if (isMobile) {
            console.log("redirecting to Mobile");
            return reply.file("public/m.index.html");
        } 
        console.log("Desktop Site");
        return reply.file("public/index.html");
    }

function emailQueryHandler(request, reply) {
    var todayTime = new Date();
    request.payload.time = todayTime.toString();
    mailer("emailQuery", request.payload, function(err) {
        if (!err) {
            reply({
                err: err,
                msg: "Thanks, We shall get back to you shortly"
            });
        } else {
            errors.relayError({
                msg: 'Error in sending Mail query',
                err: err
            });
            reply({
                err: err
            });
        }
    });

    //Save the query
    var queries = db.collection("queries");
    queries.insert(request.payload, function(err, res) {

    });
}

function couponHandler(request, reply) {
    switch (request.method) {
        case "get":
            couponUtil.fetchCoupons(request.query, function(err, res) {
                if (!err) {
                    reply({
                        data: res
                    }).code(200);
                } else {

                    reply({
                        err: err
                    }).code(404);
                    errors.relayError({
                        msg: 'Error in Fetching Coupons',
                        err: err
                    });
                }
            });
            break;
        case "post":
            //Decode Expiry
            var expiryDateArray = request.payload.coupons.expiry.split("/");
            request.payload.coupons.expiry = new Date(expiryDateArray[2], expiryDateArray[1], expiryDateArray[0]);
            couponUtil.insertCoupon(request.payload.coupons, function(err, res) {
                if (!err) {
                    reply({
                        msg: "Coupon Inserted"
                    });
                } else {
                    reply({
                        err: err
                    });
                    errors.relayError("err", {
                        msg: 'Error in inserting Coupons',
                        err: err
                    });
                }
            });
            break;

        default:
            // statements_def
            break;

    }
}

function shareCreatedStarHandler(request, reply) {
    // Handle Email

    var stars = db.collection("stars");
    stars.findOne({ "name": request.payload.starName }, function(err, res) {
        if (err) {
            console.log("cant find the start");
            reply({
                err: "Star to share not valid",
                msg: "Something went wrong"
            });
        } else {
            //Send Email
            setTimeout(function() {
                res.emailToShareWith = request.payload.emailToShareWith;
                mailer("shareCreatedStar", res, function(err, res) {
                    if (err) {
                        reply({
                            err: "err from mailer",
                            msg: "Something went wrong"
                        });
                    } else {
                        reply({
                            err: null,
                            msg: "Email has been sent."
                        });
                    }
                });

            }, settings.timeout);

        }
    });

}

function staticDataHandler(request, reply) {

    var fileToRead;
    var replyObj = {};

    switch (request.query.staticType) {
        case "terms":
            replyObj.heading = "Terms and Conditions";
            break;
        case "privacy":
            replyObj.heading = "Privacy Policy";
            break;
        case "what":
            replyObj.heading = "What is Galaxy of Love";
            break;
        case "why":
            replyObj.heading = "Why Galaxy of Love?";
            break;
    }

    replyObj.content = fs.readFileSync(path.join(__dirname, "../Content/", request.query.staticType + ".txt"), "utf8");
    reply(replyObj);
}

function starHandler(request, reply) {
    var enabled = true;
    if (request.query.payment_request_id || request.query.paymentId || request.query.PayerID || request.query.token) {
        enabled = false;
    }
    var stars = db.collection("stars");
    var query = { name: request.query.starName, enabled: enabled };
    if (!enabled && (request.query.payment_request_id || request.query.paymentId)) {
        query["payment.request_id"] = request.query.payment_request_id || request.query.paymentId;
    }
    stars.findOne(query, function(err, updatedStar) {
        setTimeout(function() {
            if (updatedStar) {
                //Got some Data reply back accordingly
                if (updatedStar.user.fbData) {
                    delete updatedStar.user.fbData;
                }
                delete updatedStar._id;
                reply({
                    starData: updatedStar,
                    error: null
                });
            } else {
                // No data send back Error
                reply({
                    error: {
                        code: 404,
                        msg: "No Such StarName"
                    }
                });

            }
        }, settings.timeout);

    });


}

function verifyPayment(request, reply) {
    // Verify Payment from InstaMojo and then reply
    console.log("verify payment for request query : ", request.query);
    helpers.payment.verifyPayment(request.query, function(error, payment_body) {
        if (!error) {
            var updateObject = {
                "payment.payment_id": request.query.payment_id || request.query.PayerID,
                "payment.payment_body": payment_body,
                enabled: true,
                'payment.source': request.query.payment_id ? "Instamojo" : "Paypal"
            };


            db.collection("stars").update({
                "name": request.query.starName
            }, {
                $set: updateObject
            }, function(err) {

                if (!err) {
                    console.log("verifying Payment");
                    reply({
                        err: null,
                        buttonMsg: "Awesome",
                        msg: "Thank you for posting the Message! \n\nYour star '" + request.query.starName + "' has been named and is shining bright."
                    });
                    // Send Welcome Email
                    db.collection("stars").findOne({ name: request.query.starName }, function(err, updatedStar) {
                        if (!err) {
                            mailer("welcome", updatedStar, function(err) {
                                if (err)
                                    console.log(err);
                                // Send email to adming for approval
                                mailer("adminNewStar", updatedStar, function(err) {
                                    if (err) {
                                        console.log(err);
                                    }
                                });
                            });
                            // Handle Coupon Codes if any
                            if (updatedStar.starPrice.couponCode && updatedStar.starPrice.couponCode != "") {
                                redemptionObject = {
                                    by: updatedStar.name,
                                    time: updatedStar.creationDate
                                };
                                db.collection("coupons").update({ "code": updatedStar.starPrice.couponCode }, { $inc: { "count": -1 }, $push: { "redeemed": redemptionObject } }, function(err, count, status) {
                                    if (err) {
                                        errors.relayError({
                                            msg: "Coupons updation  failed",
                                            err: err
                                        });
                                    }
                                });
                            }
                        } else {
                            errors.relayError({
                                msg: "Unable to find Star to send Welcome Email upon Payment Verification",
                                err: err
                            });
                        }
                    });

                } else {
                    reply({
                        err: 'Something went wrong, Please try again',
                        buttonMsg: 'OK'
                    });

                    errors.relayError({
                        msg: "Unable to insert in DB",
                        err: err
                    });
                }
            });
        } else {
            reply({
                err: error,
                msg: "We were unable to verify your payment. If money got deducted from your account, it would be refunded in 3 - 4 working days.\n\n You can always reach us at : hi@galaxyoflove.co",
                buttonMsg: "OK"

            });
            errors.relayError({
                msg: "Unable to get Payment verification from ",
                err: error
            });
            // reply({
            //     err: null,
            //     buttonMsg: "Awesome",
            //     msg: "Thank you for posting the Message! \n\nYour star '" + request.query.starName + "' has been named and is shining bright."
            // })
        }

    });

}

function createStar(request, reply) {
    console.log("Got create Star request");

    var stars = db.collection("stars");
    var updatedStar = request.payload.starData;
    var paymentData = request.payload.paymentData;
    var captchaResponse = request.payload.captchaResponse;
    var refObject;

    //Validate Star price, width and value before going to capturing payment
    var currency = updatedStar.starPrice.currency == "$" ? 'US' : "IN";

    settings.starRadius[currency].forEach(function(radiusObject) {
        if (radiusObject.radius == updatedStar.starPrice.radius) {
            refObject = radiusObject;
        }
    });

    // Validate the star

    helpers.validateStarCost(updatedStar, refObject, function(err, starCost) {
        if (!err) {
            helpers.verifyCaptcha(request.info.remoteAddress, captchaResponse, function(err) {
                if (!err) {
                    if (starCost > 0) {
                        // Get Payment request Id from Instamojo and proceed with creating a temporary Star
                        helpers.payment.getPaymentUrl(currency, updatedStar, function(err, payment_request) {
                            if (!err) {
                                //redirect to the Instamojo/Paypal Payment gateway to process payment for payment sucess
                                var paypal_url;
                                if (currency === 'US') {
                                    for (i = 0; i < payment_request.links.length; i++) {
                                        if (payment_request.links[i].rel === "approval_url") {
                                            paypal_url = payment_request.links[i].href;
                                        }
                                    }

                                }
                                insertStar(false, payment_request.id);
                                var replyObj = {
                                    url: currency === "US" ? paypal_url : payment_request.longurl,
                                    err: err,
                                    redirect: true
                                };
                                reply(replyObj);

                            } else {
                                reply({
                                    err: true,
                                    msg: "Something went wrong, please try again.",
                                    buttonMsg: "OK"
                                });
                                console.log(err);
                            }
                        });




                    } else {
                        insertStar(true, null, function(replyObj) {
                            reply(replyObj);
                        });
                    }

                } else {
                    reply({
                        err: true,
                        msg: "You don't seem to be a Human, what you are doing in the Galaxy?",
                        buttonMsg: "OK"
                    });
                    errors.relayError({
                        msg: 'Verify Catcha fail',
                        err: err
                    });
                }
            });
        } else {
            reply({
                err: true,
                msg: "Something Went wrong, please try again later.",
                buttonMsg: "OK"
            });

            errors.relayError({
                msg: 'Something went wrong while validating the Star cost during star creation',
                err: err
            });

        }
    });




    function insertStar(enabled, payment_request_id, cb) {

        updatedStar.enabled = enabled;
        updatedStar.creationDate = new Date();
        updatedStar.payment = {
            request_id: payment_request_id,
        };

        if (updatedStar.message.length > 200) {
            var tempArray = updatedStar.message.substring(0, 220).split(" ");
            updatedStar.smallMessage = tempArray.slice(0, tempArray.length - 1).join(" ") + "...";
        } else {
            updatedStar.smallMessage = updatedStar.message + "...";
        }

        stars.insert(updatedStar, function(err, res) {
            if (err) {
                console.log(err);
                if (cb) {
                    reply({
                        err: true,
                        msg: "Something Went wrong, please try again.",
                        buttonMsg: "OK"
                    });
                }
            } else {
                // Now send get Google short URl and update the star Data
                console.log(updatedStar.name + " inserted, getting shortURL");
                helpers.getShortUrl(updatedStar.name, function(shortUrl) {
                    if (updatedStar.user.fbData) {
                        delete updatedStar.user.fbData;
                    }
                    delete updatedStar._id;
                    if (cb) {
                        cb({
                            err: false,
                            msg: "Thank you for posting the Message! \n\nYour star '" + updatedStar.name + "' has been named and is shining bright.",
                            buttonMsg: "Awesome",
                            shortUrl: shortUrl,
                            redirect: false,
                            userName: updatedStar.user.name,
                            starName: updatedStar.name
                        });
                    }

                    updatedStar.shortUrl = shortUrl;
                    stars.update({ "name": updatedStar.name }, { $set: { "shortUrl": shortUrl } }, function(err, count, status) {});
                    // Send welcome Email
                    if (cb) {
                        mailer("welcome", updatedStar, function(err) {
                            if (err)
                                console.log(err);
                            // Send email to adming for approval
                            mailer("adminNewStar", updatedStar, function(err) {
                                if (err) {
                                    console.log(err);
                                }
                            });
                        });
                    }

                });

            }
        });
        //Handle Coupons If Exists
        if (cb) {
            if (updatedStar.starPrice.couponCode && updatedStar.starPrice.couponCode != "") {
                redemptionObject = {
                    by: updatedStar.name,
                    time: updatedStar.creationDate
                };
                db.collection("coupons").update({ "code": updatedStar.starPrice.couponCode }, { $inc: { "count": -1 }, $push: { "redeemed": redemptionObject } }, function(err, count, status) {
                    if (err) {
                        errors.replayError({
                            msg: "Coupons updation upon start creation failed",
                            err: err
                        });
                    }
                });
            }
        }

        removeDisabled(updatedStar.name);
    }

    function removeDisabled(starName) {
        var stars = db.collection("stars");
        console.log("Waiting 5 minutes before deleting %s, if not payment received", starName);
        setTimeout(function() {
            stars.remove({ name: starName, enabled: false }, function(err, response) {
                if (!err && response.result.n > 0) {
                    console.log("Disabled star : %s removed from DB", starName);
                } else {
                    console.log(err || "Nothing to Remove, payment has been processed for " + starName);
                }
            });
        }, 300000);


    }
}

function starNameCheck(request, reply) {
    console.log(request.query.starName);
    var stars = db.collection("stars");
    var nameSearched = request.query.starName;;
    stars.find({ "name": nameSearched }).toArray(function(err, res) {
        setTimeout(function() {
            if (err) {
                reply({
                    err: "Something went Wrong"
                });
            } else {
                if (res.length == 0) {
                    reply({
                        available: true,
                        err: null
                    });
                } else {
                    reply({
                        available: false,
                        err: null
                    });
                }
            }
        }, settings.timeout);

    });
}



function homeHandler(request, reply) {
    var currentPath = request.query.path;
    var select = {
        starImage: 1,
        smallMessage: 1,
        top: 1,
        left: 1,
        width: 1,
        name: 1,
        flare: 1,
        flareUrl: 1,
        background: 1
    };

    var stars = db.collection("stars");
    stars.find({ enabled: true }, select).toArray(function(err, results) {

        if (err) {
            console.log(err);
        } else {

            replyArray = results.map(function(obj) {
                obj.template = "public/templates/enabledPopoverTemplate.html";
                return obj;
            });
            var replyObj = {
                stars: replyArray
            };
            // Get IP Data and set the Pricing accodingly!
            helpers.getCostObj(request, function(obj) {
                replyObj.costArray = obj;
                reply(replyObj);
            });
        }
    });
}

function validateStar(request, reply) {

    var starToTest = JSON.parse(JSON.stringify(request.payload));

    var replyJson = {
        validation: true
    };
    const stars = db.collection("stars");

    console.log(starToTest);
    stars.find({ "enabled": true }).toArray(function(err, starArray) {

        var result = 1;
        if (starArray.length > 0) {

            for (var i = 0; i < starArray.length; i++) {
                var star = starArray[i];

                var topLeft = parseInt(star.left);
                var topTop = parseInt(star.top);
                var bottomRight = parseInt(star.left) + parseInt(star.width);
                var bottomBottom = parseInt(star.top) + parseInt(star.width);


                var toTestTopLeft = parseInt(starToTest.left) - parseInt(settings.starBorder);
                var toTestTopTop = parseInt(starToTest.top) - parseInt(settings.starBorder);
                var toTestBottomRight = parseInt(starToTest.left) + parseInt(starToTest.width) + parseInt(settings.starBorder);
                var toTestBottomBottom = parseInt(starToTest.top) + parseInt(starToTest.width) + parseInt(settings.starBorder);


                if (
                    ((toTestTopLeft < bottomRight && toTestTopTop < bottomBottom) && (toTestBottomRight > topLeft && toTestBottomBottom > topTop)) ||
                    (toTestBottomRight >= settings.spaceWidth) ||
                    (toTestBottomBottom >= settings.spaceHeight) ||
                    (toTestTopTop <= 0) || (toTestTopLeft <= 0) ||
                    (toTestBottomBottom >= 1500 || toTestBottomRight >= 1200)
                ) {
                    // The Star will overlap

                    result = 0;
                    break;
                }
            }
            if (result == 0) {
                replyJson.validation = false;
            }
        }
        console.log(replyJson);
        reply(replyJson);
    });
}

function validateCouponHandler(request, reply) {
    var couponCode = request.payload.couponCode;
    var coupons = db.collection("coupons");

    // Find if the Coupon is valid and then accordingly 
    // update the StarPrice and send back or send error
    helpers.validateCoupon(couponCode, function(err, value) {

        if (!err) {
            reply({
                valid: true,
                msg: "Coupon Applied",
                value: value
            });
        } else {
            reply({
                valid: false,
                msg: err
            });
        }
    });
}

function uploadFiles(request, reply) {
    console.log("Got file upload request");
    var imageType = request.params.imageType;
    var form = new multiparty.Form();
    form.parse(request.payload, function(err, fields, files) {
        fs.readFile(files.file[0].path, function(err, data) {
            var newName = randomstring.generate(10) + "." + files.file[0].originalFilename.split(".")[1];

            var newpath = path.join(__dirname, "../Temp/", newName);
            var thumbpath = path.join(__dirname, "../Temp/", "thumb_" + newName);

            fs.writeFile(newpath, data, function(err) {

                switch (imageType) {
                    case "popUpImage":
                        easyimg.convert({
                            src: newpath,
                            dst: newpath,
                            quality: 70,
                            width: 300
                        }).then(function(image) {
                            var filesToUpload = [{
                                name: newName,
                                path: newpath
                            }];

                            fileUploader(filesToUpload, "userPics", function(err) {
                                console.log(settings.userPicsServerPath + newName);
                                if (!err) {
                                    reply({
                                        url: settings.userPicsServerPath + newName
                                    });
                                } else {
                                    console.log("Some error");
                                    reply({
                                        err: "Something went wrong try again"
                                    });
                                }
                            });

                        }, function(err) {

                            reply({
                                err: "Something went wrong try again"
                            });

                        });
                        break;
                    case "msgImage":
                        // Convert the image received into a thumbnail and then send both paths to the User
                        easyimg.thumbnail({
                            src: newpath,
                            dst: thumbpath,
                            width: 150,
                            height: 150,
                            gravity: "center"
                        }).then(function(image) {
                            easyimg.info(
                                newpath
                            ).then(function(imageDetail) {

                                var imageWidth = imageDetail.width > 1200 ? 1200 : imageDetail.width;

                                easyimg.resize({
                                    src: newpath,
                                    dst: newpath,
                                    quality: 50,
                                    width: 1200

                                }).then(function() {

                                    var filesToUpload = [{
                                        name: newName,
                                        path: newpath
                                    }, {
                                        name: "thumb_" + newName,
                                        path: thumbpath
                                    }];

                                    fileUploader(filesToUpload, "userPics", function(err) {
                                        if (!err) {
                                            console.log(settings.userPicsServerPath + "thumb_" + newName);
                                            console.log(settings.userPicsServerPath + newName);
                                            reply({
                                                thumbUrl: settings.userPicsServerPath + "thumb_" + newName,
                                                url: settings.userPicsServerPath + newName
                                            });
                                        } else {
                                            console.log(err)
                                            reply({
                                                err: "Something went wrong try again"
                                            });
                                        }
                                    });

                                });
                            });

                        }, function(err) {
                            console.log(err)
                            reply({
                                err: "Something went wrong try again"
                            });
                        });

                        break;
                }
            });
        });
    });
}
