'use strict';

var mobileCheckPlugin = {
    register: function(server, options, next) {
        server.ext("onRequest", function(req, reply) {
            console.log("From Plugin")
            console.log(req)
            reply.continue();
        })
        next();
    }
};

mobileCheckPlugin.register.attributes = {
    name: 'mobileCheckPlugin',
    version: '1.0.0'
};

module.exports = mobileCheckPlugin