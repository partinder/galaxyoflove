(function() {
    require("../Utils/mongoUtil").connectToMongo(function(err, db) {
        if (!err) {
            console.log('Connected to Db')
            var stars = db.collection("stars")
            stars.find({ "user.fbData": { $exists: true } }).toArray(function(err, res) {
                if (!err) {
                    console.log(res.length)
                    updatesStars = res.map(function(star) {
                        star.user.pictureUrl = "https://graph.facebook.com/" + star.user.fbData.authResponse.userID + "/picture?height=300"
                        return star
                    })
                    updatesStars.forEach(function(newStar) {
                        stars.update({ "_id": newStar._id }, { $set: { "user.pictureUrl": newStar.user.pictureUrl } })
                    })
                }
            })
        } else {
            console.log(err)
        }
    })
})()