(function() {
    require("../Utils/mongoUtil").connectToMongo(function(err, db) {
        if (process.argv.length > 4) {
            if (!err) {
                console.log('Connected to Db')
                var stars = db.collection("stars")
                var starName = process.argv[2]
                var starSize = process.argv[3]
                var flareVersion = process.argv[4]
                var query = getQuery(starSize, flareVersion)
                var setunset = process.argv[5] ? "$unset" : "$set";

                stars.update({ name: process.argv[2] }, {
                    $set : query
                }, function(err, response, count) {
                    if (!err) {
                        console.log("Uddated")
                        console.log(response.result)
                    } else {
                        console.log(err)
                    }
                    process.exit(1)
                })
            } else {
                console.log(err)
            }

        } else {
            console.log("No Star name CLI argument passed")
        }

    })

    function getQuery(starSize, flareVersion) {
        if (starSize == "24px") {
            return {
                "background.color": "#e0e24a",
                "background.width": "200",
                "background.top": "95",
                "background.left": "89",
                "background.flare": true,
                "background.flareUrl": "//d44k5f1zplbpz.cloudfront.net/staticImages/24pxflare.png"
            }
        }

        var topLeft = (starSize == "4px") ? "48" : "47"

        if (!flareVersion) {
            flareVersion = "v1"
        }

        if (starSize == "8px" && flareVersion == "v1") {
            topLeft = "46"
        }
        return {
            "background.width": "100",
            "background.top": topLeft,
            "background.left": topLeft,
            "background.flare": true,
            "background.flareUrl": "//d44k5f1zplbpz.cloudfront.net/staticImages/" + starSize + "flare-" + flareVersion + ".png"

        }
    }
})()


// "background" : {
//      "width" : 100,
//      "top" : 47,
//      "left" : 47,
//      "flare" : true,
//      "flareUrl" : "//d44k5f1zplbpz.cloudfront.net/staticImages/8pxflare-v1.png"
//  }

// {
//     "background.width": 100,
//     "background.top": 47,
//     "background.left": 47,
//     "background.flare": true,
//     "background.flareUrl": "//d44k5f1zplbpz.cloudfront.net/staticImages/8pxflare.png"
// }

// "background" : {
//         "color" : "#e0e24a",
//         "width" : "200",
//         "top" : "95",
//         "left" : "90",
//         "flare" : true,
//         "flareUrl" : "//d44k5f1zplbpz.cloudfront.net/staticImages/24pxflare.png"
//     }

// {
//     "background.width": 100,
//     "background.top": 48,
//     "background.left": 48,
//     "background.flare": true,
//     "background.flareUrl": "//d44k5f1zplbpz.cloudfront.net/staticImages/4pxflare-v1.png"
// }