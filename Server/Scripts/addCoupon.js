(function() {
    //Get DB Connection
    require("../Utils/mongoUtil").connectToMongo(function(err, db) {
        if (!err) {
            console.log("Connected to Db")
            var coupons = db.collection("coupons")

            var couponToInsert = {

                "code": "FREESTARPARI",
                "count": 100,
                "expiry": new Date(2018, 03, 21),
                "value": 100

            }

            coupons.insert(couponToInsert, function(err, res) {
            	if(!err){
            		console.log("Coupon code %s inserted",couponToInsert.code)
            	} else {
            		console.log(err)
            	}
                process.exit(1)
            })
        } else {
            console.log("Error Conecting to Database")
        }
    })
})()

// {
//     "code": "FREESTAR",
//     "count": 100,
//     "expiry": "21/03/2018",
//     "value": 100
// }