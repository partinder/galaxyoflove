(function() {
    require("../Utils/mongoUtil").connectToMongo(function(err, db) {
        if (!err) {
            console.log('Connected to Db')
            var stars = db.collection("stars")
            stars.find({ "starImage": { $exists: true } }).toArray(function(err, res) {
                if (!err) {
                    res.forEach(function(doc){
                    	var newImageUrl = "//"+doc.starImage.split("http://")[1]
                    	var messagePics  = []
                    	doc.messagePics.forEach(function(pic){
                    		messagePics.push({
                    			url : "//"+pic.url.split("http://")[1],
                    			thumbnail : "//"+pic.thumbnail.split("http://")[1]
                    		})
                    	})

						stars.update({"_id":doc._id},{
							$set :{
								starImage : newImageUrl,
								messagePics : messagePics
							}
						},function(err,res){
							console.log(err)
							console.log(res.result)
						})                    	

                    	//stars.update({"_id":doc._id},{$set:{"starImage":newImageUrl,"messagePics":messagePics})
                    	
                    })
                }
            })
        } else {
            console.log(err)
        }
    })
})()