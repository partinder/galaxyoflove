(function() {
    // Read the message file and split into Long and small messages
    var fs = require("fs")
    var message = fs.readFileSync("./starMessage.txt", "utf8").toString()
    var smallMessage;
    if (message.length > 220) {
        var tempArray = message.substring(0, 220).split(" ")
        smallMessage = tempArray.slice(0, tempArray.length - 1).join(" ") + "..."
    } else {
        smallMessage = message + "..."
    }
    require("../Utils/mongoUtil").connectToMongo(function(err, db) {
        if (process.argv.length > 2) {
            if (!err) {
                console.log('Connected to Db')
                var stars = db.collection("stars")
                console.log(process.argv[2])
                stars.update({ name: process.argv[2] }, {
                    $set: {
                        message: message,
                        smallMessage: smallMessage
                    }
                }, function(err, response, count) {
                    if (!err) {
                        console.log("Uddated")
                        console.log(response.result)
                    } else {
                        console.log(err)
                    }
                    process.exit(1)
                })
            } else {
                console.log(err)
            }

        } else {
            console.log("No Star name argument passed")
        }

    })
})()