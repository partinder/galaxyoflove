var mongoUtil = require("../../Server/Utils/mongoUtil")

randomIndexes = []

insertRandom()

function insertRandom() {
    var random = randomIntFromInterval(0, 1999)
    if (randomIndexes.length < 1500) {
        if (randomIndexes.indexOf(random) == -1) {
            randomIndexes.push(random)
        }
        insertRandom()
    } else {
        return
    }
}


mongoUtil.connectToMongo(function(err) {
    if (!err) {
        console.log("Connected to DB")
        var db = mongoUtil.getDb()
        var stars = db.collection("stars")
        stars.find().toArray(function(err, results) {
            if (err) {
                console.log(err)
            } else {
            	var newArray = []
                randomIndexes.forEach(function(index){
                	results[index].enabled = false
                	newArray.push(results[index])
                })

                asyncLoop(randomIndexes.length,function(loop){
                	var docToUpdate = newArray[loop.iteration()]
                	stars.update({"_id":docToUpdate._id},docToUpdate,function(err,record){
                		if(err) console.log(err)
                			loop.next()
                	})

                },function(){
                	console.log("All Done")
                })

            }
        })
    }
})

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function asyncLoop(iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}