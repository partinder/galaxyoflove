var _ = require("underscore")
var casual = require("casual")

var settings = {
    spaceWidth: 1200,
    spaceHeight: 2500,
    totalNumberOfStars: 2000,
    border: 5
}

var starsType = {
    s: { width: 4, percentage: 55 },
    m: { width: 6, percentage: 20 },
    l: { width: 10, percentage: 4 },
    xl: { width: 16, percentage: 1 },
    xs: { width: 8, percentage: 20 }
}

var starArray = []
var total = 0

var startTime = new Date()

var keys = _.keys(starsType)

module.exports = function(callback) {
    starArray = []
    asyncLoop(keys.length, function(loop) {
        console.log(keys[loop.iteration()])
        starType = starsType[keys[loop.iteration()]]
        generateStars(starType, function() {
            setTimeout(() => {
                loop.next()
            }, 500)
        })
    }, function() {
        console.log("All DONE")
        var endTime = new Date()
        console.log(starArray.length)
        console.log("Done in %s seconds", (endTime - startTime) / 3600)
        callback(starArray)
    })

}


function generateStars(starType, callback) {

    var NumberOfStars = (starType.percentage / 100) * settings.totalNumberOfStars

    asyncLoop(NumberOfStars, function(loop) {
        if (getStar(starType)) {
            starArray.push(getStar(starType))
            console.log("Array Length : ", starArray.length)
            setTimeout(function() { loop.next() }, 1)

        } else {
            console.log(getStar(starType))
            loop.next()
        }


    }, function() {
        callback()
    })

}


function randomStar() {

    return {
        left: randomIntFromInterval(0, settings.spaceWidth),
        top: randomIntFromInterval(0, settings.spaceHeight),
        enabled: true,
        id: 123456,
        name: "Hello"
    }

}

function getStar(starType) {
    var starToTest = randomStar()
    if (!starToTest) {
        console.log("ERR")
    }
    starToTest.width = starType.width

    var result = 1
    if (starArray.length > 0) {

        for (i = 0; i < starArray.length; i++) {
            var star = starArray[i]
            if (!star) {
                console.log(starArray[starArray.length - 1], starArray[starArray.length - 2])
            }

            var topLeft = star.left
            var topTop = star.top
            var bottomRight = star.left + star.width
            var bottomBottom = star.top + star.width

            var toTestTopLeft = starToTest.left - settings.border
            var toTestTopTop = starToTest.top - settings.border
            var toTestBottomRight = starToTest.left + starToTest.width + settings.border
            var toTestBottomBottom = starToTest.top + starToTest.width + settings.border

            if (
                ((toTestTopLeft < bottomRight && toTestTopTop < bottomBottom) &&  (toTestBottomRight > topLeft && toTestBottomBottom > topTop)) ||
                (toTestBottomRight >= settings.spaceWidth) ||
                ( toTestBottomBottom >= settings.spaceHeight) ||
                (toTestTopTop <= 0) || (toTestTopLeft <= 0)
            ) {
                // The Star will overlap generate a new Star
                result = 0
                break;
            }
        }
        if (result == 0) {
        	console.log(topLeft, topTop, bottomRight,bottomRight)
        	console.log(toTestTopLeft, toTestTopTop, toTestBottomRight,toTestBottomBottom)
            console.log("Call Again", starArray.length)

            	return getStar(starType)

        } else {
            return starToTest
        }
    } else {
        return starToTest
    }
}

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function testOverlap(star, starToTest) {

    const topLeft = star.left
    const topTop = star.top
    const bottomRight = star.left + star.width
    const bottomBottom = star.top + star.width

    const toTestTopLeft = starToTest.left - settings.border
    const toTestTopTop = starToTest.top - settings.border
    const toTestBottomRight = starToTest.left + starToTest.width + settings.border
    const toTestBottomBottom = starToTest.top + starToTest.width + settings.border

    if (
        (toTestTopLeft < bottomRight && toTestTopTop < bottomBottom) ||
        (toTestBottomRight > topLeft && toTestBottomBottom > topTop) ||
        (toTestBottomRight > settings.spaceWidth && toTestBottomBottom > settings.spaceHeight) ||
        (toTestTopTop < 0 && toTestTopLeft < 0)
        // (starToTest.left > star.left && starToTest.left < (star.left + star.width + settings.border) && starToTest.left > ) ||
        //    (starToTest.top > star.top && starToTest.top < (star.top + star.width + settings.border)) ||
        //    (starToTest.left > (settings.spaceWidth - starToTest.width - settings.border)) ||
        //    (starToTest.top > (settings.spaceHeight - starToTest.width - settings.border))


        // (starToTest.left < settings.border) ||
        // (starToTest.top < settings.border) 
    ) {
        return 1
    } else {
        return 0
    }
}



// function validateStar(starType) {
//     var starToTest = randomStar()
//     starToTest.width = starType.width

//     if (starArray.length > 0) {
//         var result = 1
//         starArray.forEach(function(star) {

//             if (starToTest.X > star.X && starToTest.X < (star.X + star.width) && starToTest.Y > star.Y && starToTest.Y < (star.Y + star.width) &&
//                 starToTest.X > (settings.spaceWidth - starToTest.width) && starToTest.Y > (settings.spaceHeight - starToTest.Y)) {
//                 // The Star will overlap generate a new Star
//             console.log("")
//                 result = 0
//             }
//         })

//         if (result) {
//             return starToTest
//         } else {
//             console.log("Call Again")
//             validateStar(starType)
//         }

//     } else {
//         console.log("First inserting is : ", starToTest)
//         return starToTest
//     }
// }


function asyncLoop(iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}