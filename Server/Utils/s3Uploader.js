var s3 = require("s3")
var settings = require("../Config/settings.js")
var async = require("async")
var fs = require("fs")

var client = s3.createClient({
    maxAsyncS3: 20, // this is the default 
    s3RetryCount: 3, // this is the default 
    s3RetryDelay: 1000, // this is the default 
    multipartUploadThreshold: 20971520, // this is the default (20 MB) 
    multipartUploadSize: 15728640, // this is the default (15 MB) 
    s3Options: {
        accessKeyId: settings.awsAccessKeyId,
        secretAccessKey: settings.awsSecretAccessKey,
        region: 'us-west-2'
    }
});


module.exports = function(filesToUpload, fileType, callback) {
    var parallelUploads = []

    filesToUpload.forEach(function(file) {
        parallelUploads.push(
            function(callback) {
                var params = {
                    localFile: file.path,

                    s3Params: {
                        Bucket: "galaxyoflove.co",
                        Key: fileType + "/" + file.name,
                    }
                };
               
                var uploader = client.uploadFile(params);
                
                uploader.on('error', function(err) {
                    console.error("unable to upload:", err.stack);
                    callback(err)
                });
                uploader.on('progress', function() {
                    console.log("progress", uploader.progressMd5Amount,
                        uploader.progressAmount, uploader.progressTotal);
                });
                uploader.on('end', function() {
                    console.log("done uploading");
                    callback(null)
                });

            })
    })

    async.parallel(parallelUploads, function(err, results) {
        //Delete files from local system
        filesToUpload.forEach(function(file) {
            fs.unlinkSync(file.path)
        })
        if (!err) {
            callback(null)
        } else {
            callback(err)
        }
    })



    // body...
}