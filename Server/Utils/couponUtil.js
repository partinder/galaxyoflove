(function() {

    var newCoupons = require("../Config/coupons");
    var couponFuncs = {};
    var db = require("./mongoUtil").getDb();

    var couponsCollection = db.collection("coupons");
    var todayDate = new Date();

    couponFuncs.insertCoupon = function(coupons, cb) {
            //console.log(coupons)
        couponsCollection.insert(coupons, { "ordered": false }, function(err, records) {
            if (!err) {
                return cb();
            } else {
                return cb("Something went wrong, coupon not inserted");
            }
        });
    };
    couponFuncs.fetchCoupons = function(query, cb) {
        var dbQuery;
        var queryString = query.search_by;
        var queryOperator = "$" + query.operator;

        switch (query.search_by) {
            case "code":
                if (query.value == -1) {
                    dbQuery = {};
                } else {
                    dbQuery = {
                        code: query.value
                    };
                }
                break;
            case "count":
                if (query.operator == "gte") {
                    dbQuery = {
                        count: {
                            $gte: parseInt(query.value)
                        }
                    };

                } else if (query.operator == "lte") {
                    dbQuery = {
                        count: {
                            $lte: parseInt(query.value)
                        }
                    };
                }
                break;
            case "expiry":

                if (query.operator == "gte") {
                    dbQuery = {
                        "expiry": {
                            $gte: query.value
                        }
                    };

                } else if (queryOperator == "lte") {
                    dbQuery = {
                        "expiry": {
                            $lte: query.value
                        }
                    };
                }

                break;

            default:
                console.log('default');
                dbQuery = {};
                break;
        }
        console.log(dbQuery);
        couponsCollection.find(dbQuery).toArray((err, res) => {
            if (err) {

                cb(err);
            } else {
                cb(null, res);
            }
        });
    };
    couponFuncs.deleteCoupons = function() {};

    module.exports = couponFuncs;
})();