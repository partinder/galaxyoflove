(function() {
    var settings = require("../Config/settings")
    var Joi = require("joi")
    module.exports = {
        starData: Joi.object({
            top: Joi.number().min(0).max(1500).required(),
            left: Joi.number().min(0).max(1200).required(),
            width: Joi.number().min(settings.starRadius["IN"][0]["radius"]).max(settings.starRadius["IN"][settings.starRadius["IN"].length - 1]["radius"]).required(),
            messagePics: Joi.array().min(0).max(4),
            starSizeName: Joi.string(),
            starPrice: Joi.object({
                radius: Joi.number().required(),
                value: Joi.number().required(),
                currency: Joi.string().required(),
                starSize: Joi.string(),
                starPriceLabel: Joi.string(),
                couponCode: Joi.string().allow(null),
                revisedValue: Joi.number().allow(null)
            }),
            user: Joi.object({
                name: Joi.string().required(),
                pictureUrl: Joi.string().required(),
                email: Joi.string().email().required(),
                fbData: Joi.any()
            }),
            starImage: Joi.string().required(),
            name: Joi.string().min(3).max(15).required(),
            message: Joi.string().required()
        }),

        paymentData: Joi.object({
            razorpay_payment_id: Joi.string()
        }).allow(null),

        captchaResponse: Joi.string().required(),

        coupons: Joi.object({
            code: Joi.string().required(),
            value: Joi.number().required(),
            expiry: Joi.string().regex(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/g).required(),
            count: Joi.number().required()
        }),
        validateStar: Joi.object({
            top: Joi.number().min(0).max(1500).required(),
            left: Joi.number().min(0).max(1200).required(),
            width: Joi.number().min(settings.starRadius["IN"][0]["radius"]).max(settings.starRadius["IN"][settings.starRadius["IN"].length - 1]["radius"]).required()
        }),
        homeQuery: Joi.string().valid("home").valid("name-a-star").required(),
        starName: Joi.object({
            starName: Joi.string().alphanum().min(3).max(15).required(),
            payment_id: Joi.string().allow(null),
            payment_request_id: Joi.string().allow(null),
            paymentId: Joi.string().allow(null),
            PayerID: Joi.string().allow(null),
            token : Joi.string().allow(null)
        }).with('payment_id', 'payment_request_id').with('payment_request_id', 'payment_id').with('paymentId', 'PayerID').with('PayerID', "paymentId"),
        couponQuery: Joi.object({
            search_by: Joi.string().required(),
            value: Joi.string().alphanum().required(),
            operator: Joi.string().allow(null),
            api_key: Joi.string().required()
        })
    }
})()