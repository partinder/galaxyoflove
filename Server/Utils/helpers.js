var settings = require("../Config/settings")
var db = require("./mongoUtil").getDb()
var request = require("request")
var UAParser = require('ua-parser-js')
var mailer = require("./mailer")
var moment = require("moment")
var error = require("./errors")
var paypal = require('paypal-rest-sdk');
var Boom = require("boom")

paypal.configure({
    'mode': settings.paypal_mode, //sandbox or live
    'client_id': settings.paypal_client_id,
    'client_secret': settings.paypal_secret
});


var helpers = {}

process.on("uncaughtException", function(err) {
    error.exception({
        msg: 'UncaughtExpection',
        err: err
    })
    console.error(err)
        //process.exit(1)
})

helpers.adminUpdate = function(data) {
    if (settings.env == "production") {
        if (data.err) {
            data.subject = "Error on the Server: URGENT ATTENTION"
        } else {
            data.subject = "Server Process Update"
        }

        mailer("adminUpdate", data, (err) => {
            if (err) {
                console.log("Err in sending email to Admin")
            }
        })
    } else {
        console.log("Err Msg : ", data.msg)
        console.log("Err data : ", data.err)
    }
}

helpers.validateStarCost = function(updatedStar, refObject, cb) {

    if (refObject.value == updatedStar.starPrice.value && refObject.radius == updatedStar.starPrice.radius) {
        if (updatedStar.starPrice.couponCode && updatedStar.starPrice.couponCode != "") {
            //Check coupon Validation
            helpers.validateCoupon(updatedStar.starPrice.couponCode, function(err, res) {
                if (!err) {
                    //Compare the Discount value and the revised value and pass the revised value to check with Payment gateway
                    var discountValue = updatedStar.starPrice.value * res / 100
                    var revisedCost = updatedStar.starPrice.value.toString().indexOf(".") == -1 ? parseInt(updatedStar.starPrice.value - discountValue) : parseFloat(updatedStar.starPrice.value - discountValue).toFixed(2)
                    if (revisedCost == updatedStar.starPrice.revisedValue) {
                        cb(null, revisedCost)
                    } else {
                        console.log("Coupon code and revised cost calculation failed, something malicious")
                        cb("Coupon code and revised cost calculation failed, something malicious")
                    }
                } else {
                    cb("Coupon code not valid, something malicious")
                }
            })
        } else {
            cb(null, updatedStar.starPrice.value)
        }
    } else {
        console.log("Star Cost and radius do not match, something malicious")
        cb("Star Cost and radius do not match, something malicious")
    }

}

helpers.verifyCaptcha = function(ip, captchResponse, cb) {

    var options = {
        "method": "POST",
        "url": "https://www.google.com/recaptcha/api/siteverify",
        "form": {
            secret: "6Ld1axAUAAAAABFaqE6TN5JIKflRsh-LNnzKINm2",
            response: captchResponse,
            remoteip: ip
        }
    }

    function callback(error, response, body) {
        var jsonBody = JSON.parse(body)
        if (error) {
            console.log(error)
            return cb(error)
        } else {
            if (jsonBody.success) {
                console.log("Captcha Verified")
                return cb()
            } else {
                return cb("Some Error")
            }
        }
    }

    request(options, callback)



}



helpers.shuffle = function(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

helpers.getShortUrl = function(starName, cb) {

    var longUrl = "http://www.galaxyoflove.co/star/" + starName

    var options = {
        "method": "POST",
        "url": "https://www.googleapis.com/urlshortener/v1/url?key=" + settings.googleApiKey,
        "json": { longUrl: longUrl }
    }

    function callback(error, response, body) {
        if (error) {
            console.log(error)
        } else {
            cb(body.id)
        }
    }

    request(options, callback)
}

helpers.ensureLatestBrowser = function(req, reply) {
    var parser = new UAParser();
    var ua = req.headers['user-agent'];
    var browserName = parser.setUA(ua).getBrowser().name;
    if (browserName) {
        var fullBrowserVersion = parser.setUA(ua).getBrowser().version;
        var browserVersionNumber = Number(fullBrowserVersion.split(".", 1).toString())
         console.log(browserName,browserVersionNumber)
        if (browserName == 'IE' && browserVersionNumber <= 11)
            reply.redirect('incompatible');
        else if (browserName == 'Firefox' && browserVersionNumber <= 24)
            reply.redirect('incompatible');
        else if (browserName == 'Chrome' && browserVersionNumber <= 29)
            reply.redirect('incompatible');
        else if (browserName == 'Canary' && browserVersionNumber <= 32)
            reply.redirect('incompatible');
        else if (browserName == 'Safari' && browserVersionNumber <= 5)
            reply.redirect('incompatible');
        else if (browserName == 'Opera' && browserVersionNumber <= 16)
            reply.redirect('incompatible');
        else
            return reply.continue();
    } else {
        // 
        return reply.continue();
    }
}

helpers.razorPay = {
    //"""pay_6z4K8ErLTi6Qt3"""
    capture: function(razorPayId, amount, callback) {

        var options = {
            "method": "POST",
            "url": "https://" + settings.razorPayKey + ":" + settings.razorPaySecret + "@api.razorpay.com/v1/payments/" + razorPayId + "/capture",
            "form": { amount: amount }
        }
        var cb = function(err, response, body) {
            var jsonBody = JSON.parse(body)

            if (err) {
                console.log(err)
                return callback(err)

            } else {
                if (response.statusCode == 200 && jsonBody.captured == true && !jsonBody.error_code) {
                    console.log("All Good")
                    return callback()
                } else {
                    console.log(jsonBody)
                    return callback("Something went wrong")
                }
            }
        }

        request(options, cb)

    }
}

helpers.validateCoupon = function(couponCode, cb) {

    var coupons = db.collection("coupons")
    var todayDate = moment()

    coupons.findOne({
        "code": couponCode
    }, function(err, res) {
        if (!err && res) {
            // Coupon Available

            var expiryDate = moment(res.expiry)
            var valid = expiryDate.isAfter(todayDate)
            if (res.count > 0 && valid) {
                return cb(null, res.value)
            } else {
                return cb("Coupon has expired")
            }
        } else {
            return cb("Coupon not valid")
        }
    })
}

// helpers.getCostObj = function(req, callback) {

//     returnObj = settings.starRadius["IN"]
//     return callback(returnObj)
// }
helpers.getCostObj = function(req, callback) {
    returnObj = settings.starRadius["US"]
        //return callback(returnObj)
    var remoteAdd = req.headers["x-real-ip"] || req.info.remoteAddress


    var requestUrl = "http://freegeoip.net/json/" + remoteAdd
        // if (settings.env === "local") {
        //     requestUrl = "http://freegeoip.net/json/" + "72.229.28.185"
        //         // 125.99.100.73 - India
        //         // 72.229.28.185 - US
        // }
    var returnObj;
    request(requestUrl, (err, res, body) => {
        if (!err && body) {
            body = JSON.parse(body)
            if (!err && body.country_code === "IN") {
                returnObj = settings.starRadius["IN"]
            }
        }
        callback(returnObj)
    })
}

helpers.authFuncs = {
    restrict: function(req, reply) {

        if (req.url.path != "/incompatible") {
            return helpers.ensureLatestBrowser(req, reply)
        } 
        return reply.continue()
    },

    authCheck: function(req, reply) {

        var keyName = {
            client: "apikey",
            server: "golApiGetKey"
        }

        if (req.method == "post") {
            keyName.client = "apisecret"
            keyName.server = "golApiPostKey"
        }

        if (!req.headers[keyName.client]) {
            console.log(req.headers)
            reply(Boom.badRequest('Invalid Request'))
        } else {
            if (req.headers[keyName.client] != settings[keyName.server]) {
                reply(Boom.unauthorized('Invalid Api Key'))
            } else {
                reply.continue()
            }
        }
    },

    couponAuthCheck: function(req, reply) {
        var apiKey = settings.couponApiKey
        if (req.query.api_key === apiKey) {
            reply.continue()
        } else {
            reply(Boom.unauthorized("InavlidA Api Key"))
        }
    }

}

helpers.payment = {

    getPaymentUrl: function(currency, starData, cb) {

        if (currency == "IN") {
            var options = {
                method: "POST",
                uri: 'https://' + settings.insta_host + '.instamojo.com/api/1.1/payment-requests/',

                headers: {
                    'X-Api-Key': settings.insta_api_key,
                    'X-Auth-Token': settings.insta_auth_key
                },
                form: {
                    buyer_name: starData.user.name,
                    email: starData.user.email,
                    purpose: starData.starPrice.starSize.toString() + " star - " + starData.name.toUpperCase(),
                    amount: starData.starPrice.revisedValue || starData.starPrice.value,
                    redirect_url: settings.serverUrl + "/star/" + starData.name
                }
            }

            function callback(err, response, body) {
                if (!err) {
                    body = JSON.parse(body)
                    if (!body.success) {
                        err = body
                    }
                } else {
                    return cb(err)
                }
                return cb(err, body.payment_request)
            }

            request(options, callback)
        }
        if (currency == "US") {
            var create_payment_json = {
                "intent": "sale",
                "payer": {
                    "payment_method": "paypal"
                },
                "redirect_urls": {
                    "return_url": settings.serverUrl + "/star/" + starData.name,
                    "cancel_url": settings.serverUrl + "/star/" + starData.name
                },
                "transactions": [{
                    "item_list": {
                        "items": [{
                            "name": starData.starPrice.starSize + " star",
                            "sku": "star",
                            "price": starData.starPrice.revisedValue || starData.starPrice.value,
                            "currency": "USD",
                            "quantity": 1
                        }]
                    },
                    "amount": {
                        "currency": "USD",
                        "total": starData.starPrice.revisedValue || starData.starPrice.value
                    },
                    "description": "Payment for a " + starData.starPrice.starSize + " star in the Galaxy of Love."
                }]
            };

            paypal.payment.create(create_payment_json, function(error, payment) {
                if (error) {
                    cb(error)
                } else {
                    return cb(null, payment)
                }
            });
        }

    },

    verifyPayment: function(query, cb) {
        console.log("verify Payment")

        if (query.payment_id) {
            var options = {
                method: "GET",
                uri: 'https://' + settings.insta_host + '.instamojo.com/api/1.1/payments/' + query.payment_id,

                headers: {
                    'X-Api-Key': settings.insta_api_key,
                    'X-Auth-Token': settings.insta_auth_key
                }
            }

            function callback(err, response, body) {
                if (!err) {
                    body = JSON.parse(body)
                    if (body.payment && body.payment.status === 'Credit') {
                        console.log("Payment verified for Payment Id : " + query.payment_id)
                        return cb(null, body.payment)
                    } else {
                        return cb("Something didnt work out")
                    }
                }
                return cb(err)

            }

            request(options, callback)

        } else if (query.paymentId && query.PayerID) {
            // get confirmation from PayPal

            var execute_payment_json = {
                "payer_id": query.PayerID
            };

            var paymentId = query.paymentId;

            paypal.payment.execute(paymentId, execute_payment_json, function(err, payment) {
                if (!err) {
                    console.log(err, payment)
                    if (payment && payment.state === "approved") {
                        return cb(null, payment)
                    } else {
                        return cb("Something didnt work out")
                    }
                }
                return cb(err)

            });
        } else {
            return cb("No payment data Available")
        }



    }

}


module.exports = helpers
