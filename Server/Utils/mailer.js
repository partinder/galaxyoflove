var EmailTemplate = require('email-templates').EmailTemplate
var path = require('path')
var nodemailer = require("nodemailer")
var mongoUtil = require("../Utils/mongoUtil")
var settings = require("../Config/settings")
var helper = require("sendgrid").mail
var sg = require("sendgrid")(settings.sendgridKey)

// setup e-mail data with unicode symbols
function mailer(emailType, emailData, callback) {
    console.log("Sending " + emailType + " Email")
    var templateDir = path.join(__dirname, '../Templates', emailType)
    var emailTemplate = new EmailTemplate(templateDir)
    if (emailData.name) {
        emailData.starLink = settings.serverUrl + "/star/" + emailData.name
        emailData.name = emailData.name.toUpperCase()
    }
    emailTemplate.render(emailData, function(err, result) {
        if (err) {
            errors.relayError({
                err: "Unable to render Email Template",
                errData: err
            })
            return callback(err)
        } else {
            var mail;
            switch (emailType) {
                case "welcome":
                    mail = new helper.Mail(
                        new helper.Email("partinder@galaxyoflove.co", "Partinder / Galaxy of Love"), // From
                        "Welcome to the Galaxy of Love", // Subject
                        new helper.Email(emailData.user.email, emailData.user.name), // To
                        new helper.Content("text/html", result.html) // HTML Email
                    );
                    var reply_to = new helper.Email("info@galaxyoflove.co", "Galaxy of Love") // Reply To
                    mail.setReplyTo(reply_to)

                    break

                case "adminNewStar":
                    mail = new helper.Mail(
                        new helper.Email("stars@galaxyoflove.co", "Mailer / Galaxy of Love"), //From
                        "New Star in the Galaxy of Love", // Subject
                        new helper.Email("admin@galaxyoflove.co", "Admin / GOL"), // To
                        new helper.Content("text/html", result.html) // HTML Email
                    );
                    break

                case "adminUpdate":
                    // pass msg object and msg subject in the emailData from function call
                    mail = new helper.Mail(
                        new helper.Email("stars@galaxyoflove.co", "Mailer / Galaxy of Love"), //From
                        emailData.subject, // Subject
                        new helper.Email("admin@galaxyoflove.co", "Admin / GOL"), // To
                        new helper.Content("text/html", result.html) // HTML Email
                    );
                    break;
                case 'shareCreatedStar':
                    mail = new helper.Mail(
                        new helper.Email("stars@galaxyoflove.co", "Galaxy of Love"), // From
                        "Someone just expressed their love for you in the Galaxy of Love.", // Subject
                        new helper.Email(emailData.emailToShareWith), // To
                        new helper.Content("text/html", result.html) // HTML Email
                    );
                    // var reply_to = new helper.Email("info@galaxyoflove.co", "Galaxy of Love") // Reply To
                    // mail.setReplyTo(reply_to)
                    break;
                case 'emailQuery':
                    mail = new helper.Mail(
                        new helper.Email(emailData.email, emailData.name), // From
                        "New Query on Galaxy of Love from : " + emailData.email, // Subject
                        new helper.Email("admin@galaxyoflove.co", "Admin / GOL"), // To
                        new helper.Content("text/html", result.html) // HTML Email
                    );
                    break;
            }
            var request = sg.emptyRequest({
                method: 'POST',
                path: '/v3/mail/send',
                body: mail.toJSON(),
            });
            sg.API(request, function(error, response) {
                if (!error) { console.log(emailType + " email sent") } else {
                    console.error(response.body)
                }
                return callback(error)
            });

        }
    })
}


function sendViaTransporter(emaiId, htmlEmail, callback) {

    var smtpConfig = {
        host: 'smtp.gmail.com', // use SSL
        auth: {
            user: 'galaxyoflove08@gmail.com',
            pass: 'Bjorkrob!2'
        }
    };


    var transporter = nodemailer.createTransport(smtpConfig);

    var mailOptions = {
        from: '"Galaxy of Love" <star@mailer.galaxyoflove.co', // sender address
        to: emaiId, // list of receivers
        subject: 'Welcome to Galaxy of Love', // Subject line
        html: htmlEmail // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(err)
            return callback(error)
        }
        console.log("Email sent to" + emaiId)
        return callback()
    });
}

module.exports = mailer