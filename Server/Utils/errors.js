(function() {
    var EventEmitter = require("events").EventEmitter;
    var mailer = require("./mailer");
    var utils = require("util");

    Function.prototype.method = function(name, func) {
        if (!this.prototype[name]) {
            this.prototype[name] = func;
        }
        return this;
    };

    Function.method("inherits", function(from) {
        var F = function() {};
        F.prototype = from.prototype;
        this.prototype = new F();
    });

    function Errors() {
        EventEmitter.call(this);
    }

    Errors.inherits(EventEmitter);

    Errors.prototype.relayError = function(data) {
        data.subject = "Error on the Server: URGENT ATTENTION";
        this.emit("mail", data);
    };

    Errors.prototype.adminUpdate = function(data) {
        data.subject = "Server Process Update";
        this.emit("mail", data);
    };

    Errors.prototype.exception = function(data) {
        data.expection = true;
        data.subject = "UNCAUGHT EXPECTION: URGENT";
        this.emit('mail', data);
    };
    var error = new Errors();

    error.on("mail", function(data) {
        console.log("Err Msg : ", data.msg);
            console.log('Err Data : ', data.err);
            console.error("Err Stack :", data.errData);
        //Send email to Admin
        if (process.env.NODE_ENV === 'production') {
            mailer("adminUpdate", data, function(err) {
                console.log("Err Data sent to Admin email, Now exiting process");
                if (data.msg == "UncaughtExpection") {
                    process.exit(1);
                }
            });
        }
    });
    module.exports = error;
})();