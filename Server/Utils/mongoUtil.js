"use strict"
const mongoClient = require("mongodb").MongoClient
const settings = require("../Config/settings")
var dbUrl = "mongodb://" + settings.dbServer + "/" + settings.db

var _db

module.exports = {

    connectToMongo: function(callback) {
        console.log(dbUrl)
        mongoClient.connect(dbUrl, function(err, db) {
            _db = db
            return callback(err,db)
        })
    },

    getDb: function() {
        return _db
    }


}