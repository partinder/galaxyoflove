'use strict';

var le = require('greenlock-express').create({
    server:'https://acme-v01.api.letsencrypt.org/directory' // in production use https://acme-v01.api.letsencrypt.org/directory

    ,
    configDir: require('os').homedir() + '/letsencrypt/etc'

    ,
    approveDomains: function(opts, certs, cb) {
        opts.domains = certs && certs.altnames || opts.domains;
        opts.email = 'partinder@galaxyoflove.co' // CHANGE ME
        opts.agreeTos = true;

        cb(null, { options: opts, certs: certs });
    }

    ,
    debug: true
});

module.exports = le