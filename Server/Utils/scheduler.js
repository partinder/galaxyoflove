(function() {

    var fs = require("fs"),
        mongoClient = require("mongodb").MongoClient,
        settings = require("../Config/settings"),
        dbUrl = "mongodb://" + settings.dbServer + "/" + settings.db,
        schedule = require("node-schedule"),
        spawn = require("child_process").spawn,
        path = require("path"),
        s3Uploader = require("./s3Uploader.js"),
        helpers = require("./helpers"),
        db = require("./mongoUtil").getDb(),
        errors = require("./errors")

    module.exports = {
        mongo: schedule.scheduleJob("50 3 * * *", () => {


            var commands = {
                dump: "mongodump",
                zip: "zip"
            }

            if (settings.env == "production") {
                commands.dump = "/usr/bin/mongodump"
                commands.zip = "/usr/bin/zip"
            }
            var dumpDir = path.join(__dirname, "../Temp/")
            var zipName = new Date().toDateString() + ".zip"
            var zipPath = dumpDir + zipName
            var dbDir = dumpDir + "/" + settings.db
            var mongoDump = spawn(commands.dump, ["--db", "db_gol", "--out", dumpDir])

            mongoDump.on('close', (code) => {

                if (code === 0) {
                    var zip = spawn(commands.zip, ["-r", "-j", zipPath, dbDir])
                    zip.on("close", (code) => {
                        if (code === 0) {
                            // Zip done Upload Zip and then delete the 
                            console.log('Zip done')
                            s3Uploader([{
                                name: zipName,
                                path: zipPath
                            }], "dbBackups", function(err) {
                                if (err) {
                                    console.log("Error uploading db to S3")
                                } else {
                                    // db dump uploaded, delete the local directory
                                    require("child_process").execSync("/bin/rm -r " + dbDir)
                                    errors.adminUpdate({
                                        msg: 'db dump uploaded, local files and dir deleted for :' + new Date().toDateString(),
                                        err: null
                                    })

                                }
                            })
                        }
                    })
                    zip.on("error", (err) => {
                        console.log('Something went wrong with DB Zip')
                        errors.relayError({
                            msg: 'Something went wrong with DB Zip  at :' + new Date().toDateString(),
                            err: err
                        })

                    })
                }

            })

            mongoDump.on("error", (err) => {
                errors.relayError({
                    msg: "something went wrong with mongoDump at : " + new Date().toDateString(),
                    err: err
                })

            })
        })
        // starCheck: schedule.scheduleJob("*/100000 * * * *", () => {
        //     // Every 10 minutes check if there are any stars who's payment hasnt been completed, delete them
        //     var stars = db.collection("stars")
        //     stars.remove({
        //         enabled: false,
        //         'payment.request_id': {
        //             $exists: true
        //         }
        //     }, {
        //         multi: true,
        //         w: 1
        //     }, function(err, res) {
        //         if (err || res.result.n > 0) {
        //             console.log(err)
        //             console.log("Disbaled stars removed: ", res.result.n)
        //         }
        //     })
        // })
    }
})()