module.exports = {
    apps: [{
        name: "GOL",
        script: "./app.js",
        watch: true,
        env_development: {
            "NODE_ENV": "development",
        },
        env_production: {
            "NODE_ENV": "production"
        },
        env_local: {
            "NODE_ENV": "local"
        }
    }]
};