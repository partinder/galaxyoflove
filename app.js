(function() {
    "use strict";
    var settings = require("./Server/Config/settings");
    var mongoUtil = require("./Server/Utils/mongoUtil");
    var glob = require("glob");
    var Path = require("path");

    const Hapi = require("hapi");
    const server = new Hapi.Server({
        connections: {
            routes: {
                files: {
                    relativeTo: Path.join(__dirname, '/')
                }
            }
        }
    });

    server.connection({
        host: settings.hostServer,
        port: settings.hostPort,
    });


    mongoUtil.connectToMongo(function(err, db) {
        if (err) {
            console.log("Cannot Connect to DB");
        } else {
            var mongoDump = require("./Server/Utils/scheduler.js");
            server.register([require("inert")], (err) => {
                if (err) {
                    console.log("failed to Load Plugins");
                } else {

                    glob("Server/Routes/*.js", function(err, files) {
                        files.forEach(function(file) {
                            server.route(require("./" + file));
                        });
                    });

                    server.route({
                        method: 'GET',
                        path: '/.well-known/acme-challenge',
                        handler: function(request, reply) {
                            var req = request.raw.req;
                            var res = request.raw.res;
                            reply.close(false);
                            acmeResponder(req, res);
                        }
                    });

                    server.start((err) => {
                        if (err)
                            console.log(err);
                        else {
                            console.log("Server Started at port : %s:%s on %s environment", settings.hostServer, settings.hostPort, settings.env);
                            console.log(process.env.NODE_ENV);
                        }
                    });

                }
            });
        }
    });

})();



// {
//     "apps": [{
//         "name": "gol",
//         "script": "./app.js",
//         "watch": true,
//         "env_development": {
//             "NODE_ENV": "development"
//         },
//         "env_production": {
//             "NODE_ENV": "production"
//         }
//     }]
// }
